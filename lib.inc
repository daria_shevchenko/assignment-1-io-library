%define SYSCALL_EXIT 60
%define    WRITE   1
%define    STDOUT  1
section .text
 
 
; Принимает код возврата и завершает текущий процесс
exit: 
    xor rax, SYSCALL_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
string_length:
    xor rax, rax
    .counter:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .counter
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
print_string:
    push rdi
    call string_length
    pop rdi
    mov  rdx, rax; количество символов в rdx
    mov  rsi, rdi; символ в регистр rsi
    mov  rax, WRITE
    mov  rdi, STDOUT
    syscall
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    ; Принимает код символа и выводит его в stdout
    print_char:
        push rdi ; сдвигаем стек на тот символ, который надо печатать
        mov rsi, rsp ; символ в rsi
        pop rdi ; двигаем обратно
        mov rax, WRITE
        mov rdi, STDOUT
        mov rdx, 1
        syscall
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
print_int:
    test rdi, rdi ; проверяем знак
    jns print_uint ; если положительное, то выводим
    push rdi    ; значение в стек
    mov rdi, '-' ; минус в регистр
    call print_char ; печатаем минус
    pop rdi
    neg rdi ; меняем знак
    
; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
print_uint:
    mov rax, rdi
    mov r11, 10
    push 0
    .loop:
        xor rdx, rdx    
        div r11         ;деление на 10
        add dx, '0'     ;перевод в ASCII
        push rdx        ;в стек остаток
        cmp rax, 0      ;проверяем на ноль
        jne .loop  
    .print_number:
        pop rax         ;проверяем на конец строки
        cmp al, 0       
        je .exit

        mov rdi, rax    
        call print_char        ;вызов вывода символа
        jmp .print_number      ;выводим следующий символ

    .exit:
        ret



; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
string_equals:
     xor rax, rax  ;результат
     xor rdx, rdx  ;длина строки             
    .loop:           
        mov cl, [rsi+rax]
        cmp cl, [rdi+rax]       ;загружаем и сравниваем символы
        jne .noequal            ; если разные, то возвращаем 0
        test cl, cl             
        je .equal           
        inc rax
        jmp .loop   
    .equal:
        mov rax, 1  
        ret
    .noequal:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    mov rax, 0
    mov rdi, 0
    push 0
    mov rdx, 1 ;количество символов для чтения
    mov rsi, rsp ;  rsi устанавливается на вершину стека, указывая на буфер, где будет сохранен введенный символ
    syscall 
    pop rax
    ret
; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push r14
    push r15
    xor    r14, r14
    mov    r15, rsi   
    dec    r15

    .white:
        push rdi
        call read_char      ;читаем символ
        pop rdi

        cmp rax, 020h       ;проверяем пробел, табуляцию, перевод строки
        je .white
        cmp rax, 009h      
        je .white
        cmp rax, 0Ah       
        je .white

        test rax, rax
        jz .end

    .chars:
        mov byte[rdi+r14], al   ;символ в буфер
        inc r14
        push rdi            
        call read_char      ;читаем символ
        pop rdi
        cmp rax, 0          ;проверяем на окончание строки, пробел, табуляцию, перевод строки
        je .end
        cmp rax, 020h       
        je .end
        cmp rax, 009h     
        je .end
        cmp rax, 0Ah        
        je .end

        cmp r14, r15       ;проверка, помещается ли в буфер
        jg .too_long

        jmp .chars     
    .too_long:
        xor rax, rax        ;0, если вышли
        jmp .ret
    .end:
        mov byte[ rdi + r14 ], 0 ; дописываем нуль-терминатор
        mov rdx, r14        
        mov rax, rdi       
    .ret:
        pop r15
        pop r14
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rcx, 0
    mov r9, 0 
    mov r10, 10 ; используем 10-ную сс
    .loop:
        mov cl, [rdi+r9] ; читаем символ
        cmp cl,  '0'    ; если меньше нуля то выходим
        jl  .end
        cmp cl,  '9'    ; если больше девяти то выходим
        jg  .end
        sub cl,  '0'    ; вычитаем 0 , чтобы получить число
        mul r10         ; умножаем на 10, чтобы учесть новую цифру
        add rax, rcx    ; прибавляем rcx, чтобы учесть предыдущие цифры
        inc r9
        jmp .loop
    .end:
        mov rdx, r9
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    xor rax, rax
    mov cl, [rdi]
    cmp rcx, '-'            ;если положительное, то переходим на .positive
    jne parse_uint
    .neg:
        inc rdi             ;убираем знак и обрабатываем как беззнаковое
        call parse_uint 
        test rdx, rdx
        jz .ret
        inc rdx             ;возвращаем обратно
        neg rax             
        jmp .ret
    .ret:
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
string_copy:
    xor rax, rax
    .loop:
        cmp rax, rdx                    ;если больше чем буфер то выходим
        jge .exit
        mov r8b, byte [rdi+rax]
        mov byte [rsi+rax], r8b         ;запись символа
        cmp byte [rsi+rax], 0           ;проверяем на окончание 
        inc rax                         ;увеличиваем длину 
        jne .loop 
        ret
    .exit:
        xor rax, rax
        ret
